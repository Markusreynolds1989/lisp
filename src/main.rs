use std::io::Write;

fn main() {
    let mut string_input = String::new();
    //let mut index = 0;
    let mut output: Vec<Token> = Vec::new();
    let mut index: usize = 0;

    // Print.
    print!("lisp > ");
    std::io::stdout().flush().expect("Failed to flush");
    std::io::stdin()
        .read_line(&mut string_input)
        .expect("Failed to read");

    let mut input = string_input.chars().collect::<Vec<char>>();

    loop {
        // Primitives - naive.
        if input[index] != '(' {
            if input[index] == '"' {
                process_string(&mut index, &mut input, &mut output);
            }

            if input[index].is_numeric() {
                process_number(&mut index, &mut input, &mut output);
            }

            if input[index] == 't' || input[index] == 'f' {
                process_bool(&mut index, &mut input, &mut output);
            }

            // Check this last to see if the item is anything else.
            if input[index] == '\'' {
                process_symbol(&mut index, &mut input, &mut output);
            }

            if output[0].item_type == ItemType::String {
                println!("{:?}: \"{}\"", &output[0].item_type, &output[0].data);
            } else {
                println!("{:?}: {}", &output[0].item_type, &output[0].data);
            }
        }

        // Expressions - naive.
        if input[index] == '(' {
            index += 1;
            while input[index] != ')' {
                // Print returns nothing, so we can print out ();
                process_keyword(&mut index, &mut input, &mut output);
                index += 1;
                process_number(&mut index, &mut input, &mut output);
            }

            if output[0].item_type == ItemType::Function {
                if output[0].data == "Print" {
                    println!("{}", output[1].data);
                }
            }
        }

        string_input.clear();
        output.clear();
        index = 0;

        // Print.
        print!("lisp > ");
        std::io::stdout().flush().expect("Failed to flush");

        std::io::stdin()
            .read_line(&mut string_input)
            .expect("Failed to read");

        input = string_input.chars().collect::<Vec<char>>();
    }
}

fn process_keyword(index: &mut usize, input: &mut Vec<char>, output: &mut Vec<Token>) {
    let mut temp = String::new();

    while input[*index] != ' ' {
        temp.push(input[*index]);
        *index += 1;
    }

    if temp == format!("{:?}", KeyWords::Print) {
        output.push(Token {
            item_type: ItemType::Function,
            data: temp,
        })
    }
}

fn process_string(index: &mut usize, input: &mut Vec<char>, output: &mut Vec<Token>) {
    let mut temp = String::new();

    *index += 1;
    while input[*index] != '"' {
        temp.push(input[*index]);
        *index += 1;
    }

    output.push(Token {
        item_type: ItemType::String,
        data: temp,
    });
}

fn process_number(index: &mut usize, input: &mut Vec<char>, output: &mut Vec<Token>) {
    let mut temp = String::new();
    //TODO: Regex to check for only 1 . .
    while input[*index].is_numeric() || input[*index] == '.' {
        temp.push(input[*index]);
        *index += 1;
    }

    output.push(Token {
        item_type: ItemType::Number,
        data: temp,
    })
}

fn process_bool(index: &mut usize, input: &mut Vec<char>, output: &mut Vec<Token>) {
    if input[*index + 1] == '\r' || input[*index + 1] == ' ' {
        output.push(Token {
            item_type: ItemType::Boolean,
            data: input[*index].to_string(),
        })
    }
}

fn process_symbol(index: &mut usize, input: &mut Vec<char>, output: &mut Vec<Token>) {
    let mut temp = String::new();
    *index += 1;
    while input[*index] != '\r' {
        temp.push(input[*index]);
        *index += 1;
    }
    output.push(Token {
        item_type: ItemType::Symbol,
        data: temp,
    })
}

#[derive(Debug, PartialEq)]
enum ItemType {
    String,
    Number,
    Boolean,
    Symbol,
    Empty,
    Function,
}

struct Token {
    item_type: ItemType,
    data: String,
}

#[derive(Debug)]
enum KeyWords {
    Let,
    Print,
    Eval,
    Exit,
    Add,
}
